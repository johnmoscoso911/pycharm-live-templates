# Editor > Live Templates

[![N|Solid](https://d3nmt5vlzunoa1.cloudfront.net/pycharm/files/2015/12/PyCharm_400x400_Twitter_logo_white.png)](https://www.jetbrains.com/pycharm/)

# Features!

  - action
  - checkconstraints
  - commonviews
  - defcreate
  - defwrite
  - fchar
  - fdate
  - fdatetime
  - ffloat
  - fint
  - fmany2many
  - fmany2one
  - fone2many
  - fselection
  - ftext
  - imports
  - inherit
  - inheritview
  - manifest
  - model
  - name_get
  - name_search
  - newxml
  - onchange
  - recorddata
  - sqlconst

### Installation

```sh
$ cd ~/.PyCharm<VERSION>/config/templates
$ git clone https://johnmoscoso911@bitbucket.org/johnmoscoso911/pycharm-live-templates.git
$ git pull
```
